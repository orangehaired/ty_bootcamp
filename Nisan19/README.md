# Kodluyoruz - Infrastructure Cases

# Case - 1

Bu case'de bir centos kurup, bir disk mount edip, ana diskten, mount edilmiş diske bir dosya taşımamız isteniliyordu.

## Disk oluştur ve formatla

Bir sanal disk yarattık virtualbox tarafında ve bu diski centos'umuzda görmeyi bekliyoruz ki **/dev/sdb** olarak görmüş bulunduk 10 GB'ı. Bu diskimizi bir dosya sistemiyle (**ext3**) ile biçimlendirelim.

    sudo mkfs -t ext3 /dev/sdb

![Any](https://i.ibb.co/Jrq5cKC/1.jpg)

Teyit edelim biçimlendirildiğini **fdisk -l** ile;

![Any](https://i.ibb.co/yNnZkXM/1.png)

Şimdi mount edilmeye hazır.


## Disk mount

Mount edelim /bootcamp altına;

    sudo mount /dev/sdb bootcamp

![Any](https://i.ibb.co/rwW1m3H/2.png)

## /opt/bootcamp/ altında dosya oluştur ve taşı

/opt/bootcamp klasörümüzü yaratalım.

    mkdir -p /opt/bootcamp

Ardından bu dosyanın sahipliğini değiştirelim ki echo ile yönlendirdiğimiz dosyaya yazabilsin.

    sudo chown $USER:GROUP /opt/bootcamp

Şimdi dosyamızı ve içeriğimizi yaratabiliriz.

    echo 'Merhaba Trendyol =)' > /opt/bootcamp/bootcamp.txt

Son olarak **/opt/bootcamp** altında dosyamızı arayıp, bulduktan sonra mount ettiğimiz diske taşıyalım;

    sudo find /opt/bootcamp -name bootcamp.txt -exec mv -v {} /bootcamp/ \;

![Any](https://i.ibb.co/6NJ6Gkn/3.png)




# Case 2

## Variable'ları tanımla

- domain: trendyolstaticsite.com
  > **Nginx** configuration'larında kullanılacak olan host header, configuration name ve klasör ismi.
- message: "I'm from Kodluyoruz! Welcome to Trendyol DevOps site."
  > **Welcome Devops** static sayfasında render edilecek mesaj.
- access_header_key: bootcamp
  > **Welcome Devops** static sayfasına erişim için gönderilmesi gereken başlık.
- access_header_value: devops
  > **Welcome Devops** static sayfasına erişim için gönderilmesi gereken başlığın değeri.

## Templates

### Nginx configuration template
```
    server {
  listen 80;
  listen [::]:80;
  server_name {{ domain }};
  root /var/www/{{ domain }};

  location / {
  if ($http_{{ access_header_key }} != "{{ access_header_value }}") {
           proxy_pass http://localhost:5000; // Get out to dockerized app
    }

    try_files $uri $uri/ =404;
  }
}
```
> İstenilen header key ve value'si geldiği sürece **welcome-devops** sayfalarını render et aksi taktirde dockerize ettiğin container'a yönlendir.

###  Welcome Devops HTML Page
```
<!DOCTYPE html>
<html lang="en">
  <title> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://unpkg.com/tachyons/css/tachyons.min.css">
  <body>
    <article class="vh-100 dt w-100 bg-dark-pink">
      <div class="dtc v-mid tc white ph3 ph4-l">
        <h1 class="f6 f2-m f-subheadline-l fw6 tc">{{ message }}</h1>
      </div>
    </article>
  </body>
</html>
```
> Verilen mesajı render etmesi bir message variable'ı bekliyor.

## Tasks

### 1 - Install Epel Release

```
- name: Install Epel Release
  yum:
    name: epel-release
    state: latest
  become: yes
```
> Extra paketleri kurabilmek ve repoları gpgkeys, repo listini çekebilmek için bu pakete ihtiyaç duyuyoruz.

### 2 - Install yum utils
```
- name: Install yum utils
  yum:
    name: yum-utils
    state: latest
  become: yes
```
> Yum yardımcı paketini ihtiyaç duyuyoruz çünkü bir paketin kurulup kurulmadığı ona soruyoruz ya da x paketin hangi bağımlıkları var dediğimiz de kendileri cevap verir bizlere

### 3 - Install device-mapper-persistent-data

```
- name: Install device-mapper-persistent-data
  yum:
    name: device-mapper-persistent-data
    state: latest
  become: yes
```
> Docker'ın ihtiyaç duyduğu disk yardım paketi.

### 4 - Install lvm2
```
- name: Install lvm2
  yum:
    name: lvm2
    state: latest
  become: yes
```
> Volume'leri yönetebilmemiz için daha doğrusu docker'ın yönetebilmesi için bu pakete ihtiyaç duyuyoruz.

### 5- Add Docker repo
```
- name: Add Docker repo
  get_url:
    url: https://download.docker.com/linux/centos/docker-ce.repo
    dest: /etc/yum.repos.d/docer-ce.repo
  become: yes
```
> Docker paketini indirmek için GPG keylerimizi import edelim.

### 6 - Enable Docker Edge repo

```
- name: Enable Docker Edge repo
  ini_file:
    dest: /etc/yum.repos.d/docer-ce.repo
    section: 'docker-ce-edge'
  option: enabled
    value: 0
  become: yes
```
> Enable edelim repomuzu.

### 7 -  Enable Docker Test repo
```
- name: Enable Docker Test repo
  ini_file:
    dest: /etc/yum.repos.d/docer-ce.repo
    section: 'docker-ce-test'
  option: enabled
    value: 0
  become: yes
```

> Keza aynı şekilde test repomuzu da aktif edelim.

### 8 -  Install Docker
```
- name: Install Docker
  package:
    name: docker-ce
    state: latest
  become: yes
```
> Nihayet docker'ı indirmeye hazır hale geldik ve çekelim repo'dan paketimizi.

### 9 - Install docker-compose
```
- name: Install docker-compose
  get_url:
    url: https://github.com/docker/compose/releases/download/1.29.1/docker-compose-linux-x86_64
    dest: /usr/local/bin/docker-compose
    owner: root
    mode: 0755
  become: yes
```

> Docker-compose için binary'imizi çekelim ve linkleyelim /usr/bin/ altında.

### 10 - Start Docker service
```
- name: Start Docker service
  service:
    name: docker
    state: started
    enabled: yes
  become: yes
```
> Docker servisi başlatalım.

### 11 - Add atarikgunal to docker group
```
- name: Add atarikgunal to docker group
  user:
    name: atarikgunal
    groups: docker
    append: yes
  become: yes
```

> Kullanıcımızı docker grubuna ekleyelim ki container ayağa kaldırırken yetkiye ihtiyacımız kalmasın.

### 12 - Install Git

```
- name: Install Git
  package:
    name: git
    state: latest
  become: yes
```

> Dockerize edilmiş projemizi çekebilmemiz için git'e ihtiyacımız var, yükleyelim.

### 13 - Clone dockerized app with git directory
```
- name: Clone a repo with git directory
  ansible.builtin.git:
    repo: https://github.com/kartheekgottipati/Docker-compose-flask-redis-deploy
    dest: /tmp/anyapp
```
> **/tmp/anyapp** altına çekelim projemizi.

### 14 -   Wake up containers
```
- name: Wake up containers.
  community.docker.docker_compose:
    project_src: /tmp/anyapp
  vars:
    ansible_python_interpreter: /usr/bin/python3
```
> docker-compose ile container'larımızı ayağa kaldıralım.

### 15 - Install Nginx

```
- name: Install Nginx
  package:
    name: nginx
    state: latest
  become: yes
```

### 16 - Copy nginx site.conf

```
- name: Copy nginx site.conf
    template:
      src: site.conf.j2
      dest: /etc/nginx/conf.d/{{ domain }}.conf
      owner: root
      group: root
      mode: '0644'
  become: true
```
> Jinja ile render edilen site.conf'u **/etc/nginx/conf.d** altına koy.

### 17 - Create domain directory
```
- name: "Create domain directory"
  file:
    path: /var/www/{{ domain }}
    state: directory
    mode: '0775'
  become: true
```
>  Domainimize bir klasör yaratalım, onu diğer domainlerden izole edelim.

### 18 - Sync domain site
```
- name: "Sync domain site"
  template:
    src: site/index.html.j2
    dest: /var/www/{{ domain }}/index.html
  become: true
```
> Render edilen index.html'i domain klasörüne ver.

## Run ansible

### Targets
```
all:
  hosts:
  children:
    trendyol_webservers:
      hosts:
        192.168.1.34:
      vars:
        ansible_connection: ssh
        ansible_user: atarikgunal
        ansible_password: toor
        ansible_python_interpreter: /usr/bin/python3
        domain: trendyolstaticsite.com
        access_header_key: bootcamp
        access_header_value: devops
        message: "I'm from Kodluyoruz! Welcome to Trendyol DevOps site."
```
> hosts.yaml

### Dockerized App
Artık çalıştıralım ve nihâi sonuca bakalım;

    ansible-playbook templates/ty_deployment_1.yaml -i hosts.yaml -l trendyol_webservers

Ve sonuç =)

![Any](https://i.ibb.co/10n5W9N/6.png)

### Welcome Trendyol Devops Page
Welcome devops sayfası için header'ları gönderelim bakalım aksi taktirde welcome'ı render etmeyecek.
![Any](https://i.ibb.co/gybC767/5.png)

Teşekkürler task ve anlatılarınız için =) Tüm ekibe selamlar ^^