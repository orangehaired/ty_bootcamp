
# ahmettarik@linux.com - Trendyol Database Case-2
# Based https://gist.github.com/stewartadam/f59f47614da1a9ab62d9881ae4fbe656

import re, json
from urllib.parse import urlparse, urlunparse
from flask import Flask, render_template, request, abort, Response, redirect
import requests
import logging

proxy_app = Flask(__name__.split('.')[0])
info_app = Flask('info_app')
logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger("app.py")

# Hard-coded ports and creds ='(
DEFAULT_COUCHBASE_ENDPOINT = 'localhost:8091'
DEFAULT_COUCHBASE_CREDS = ('Administrator', 'password')
PROXY_PORT = 5000
INFO_PORT = 5001

@proxy_app.route('/')
def index():
    return json.dumps({
                'Name': 'Simple Couchbase HTTP Proxy API',
                'Do': 'Provide me \'/\' after Coucbase API endpoint. For example localhost:{}/pools'.format(PROXY_PORT)
            })


@proxy_app.route('/<path:url>', methods=["GET", "POST"])
def proxy(url):
    url = '{}/{}'.format(DEFAULT_COUCHBASE_ENDPOINT, url)
    r = make_request(url, request.method, dict(request.headers), request.form)
    headers = dict(r.raw.headers)
    def generate():
        for chunk in r.raw.stream(decode_content=False):
            yield chunk
    out = Response(generate(), headers=headers)
    out.status_code = r.status_code
    return out


def make_request(url, method, headers={}, data=None):
    url = 'http://%s' % url
    LOG.debug("Sending %s %s with headers: %s and data %s", method, url, headers, data)
    return requests.request(method, url, params=request.args, stream=True, headers=headers, allow_redirects=False, data=data)


# Info API side
def make_couchbase_request(endpoint):
    url = 'http://{}/{}'.format(DEFAULT_COUCHBASE_ENDPOINT, endpoint)
    r = requests.get(url, auth=DEFAULT_COUCHBASE_CREDS)
    print(r.text)
    return r.text if r.status_code == 200 else {"error": "Başaramadık abi.."}
    

@info_app.route('/')
def index():
    return json.dumps({
            'Name': 'Couchbase Info API',
        })

@info_app.route('/info/general')
def general_metadata():
    return make_couchbase_request('pools/default')

@info_app.route('/info/buckets')
def buckets_metadata():
    return make_couchbase_request('pools/default/buckets')

@info_app.route('/info/nodes')
def nodes_metadata():
    return make_couchbase_request('pools/nodes')



if __name__ == '__main__':
    import gevent
    from gevent.pywsgi import WSGIServer
    
    proxy_server = WSGIServer(('0.0.0.0', PROXY_PORT), proxy_app)
    proxy_server.start()
    print("Started Proxy server on {}".format(PROXY_PORT))

    info_server = WSGIServer(('0.0.0.0', INFO_PORT), info_app)
    info_server.start()
    print("Started Info server on {}".format(INFO_PORT))

    while True:
        gevent.sleep(60)
