# Kodluyoruz - Trendyol Database Cases

# Case - 1

Bu case'de bizden hem **SQL** hem de **key value** sorgularının çalışacağı resistant ve distributed bir couchbase clusterı design etmemiz ve çalıştırmamız istendi.

## Dockerized Couchbase?

Couchbase'in [resmi](https://blog.couchbase.com/couchbase-using-docker-compose/) kaynaklarından bir  [repo](https://github.com/arun-gupta/docker-images/tree/master/couchbase) gözümü kesti. Burdan sonraki adımlarda referans noktamız, bu reponun dökümantasyonu olacak.

Çekelim bakalım repoyu. 

    git clone https://github.com/arun-gupta/docker-images && cd couchbase
    
### Build Couchbase Offical based Custom Image 
Dockerfile'ımızdan bir image yaratalım.

    docker build -t arungupta/couchbase .

Ama öncesinde bir göz atalım Dockerfile'a.

    FROM  couchbase:latest
	COPY  configure-node.sh  /opt/couchbase
	#HEALTHCHECK --interval=5s --timeout=3s CMD curl --fail http://localhost:8091/pools || exit 1
	CMD  ["/opt/couchbase/configure-node.sh"]

Offical couchbase image'ının üstüne bir configure.sh shell scriptini salıyor. Bu scriptin yaptığı iş; couchbase'in kurulum ekranında yapılan konfigrasyonun otomatize edilmesi. Bunlar ne gibi işler? Set memory quota, set default credentails vs.

### Create Network

Görüldüğü üzere docker swarm'a service vereceğiz. Bu yüzden network tanımını manuel yapalım. Aksi taktirde servisi oluştururken hali hazırda bir **couchbase** network'ü tanımlı olmayacağı için bize kızacak ve servis yaratılamayacak.

`docker network create -d overlay couchbase`

Yaratmış olduk, artık servisimizi bağlayabileceğimiz bir network'ümğz var.

### Create Swarm Service (For Master)
`docker service create --name couchbase-master --replicas 1 -p 8091:8091 --network couchbase -e TYPE=MASTER arungupta/couchbase`

Yukarıdaki servis tanımınıda demek istiyoruz ki; az evvel build ettiğim couchbase image'ını al, ondan bir tane yarat ve onu couchbase network'üne dahil et, ardından da 8091'i host makinanın 8091'ini mapleyerek dışarı aç.

![a](https://i.ibb.co/Jp5X3SS/1.png)

E ama biz sadece bu durumda master'ı yarattık. Ama bizden bu case'de ne isteniyordu? Distributed bir system.  Hemen bir slave/node'ları da kaldıralım ayağa.

### Create Swarm Service (For Slaves/Nodes)

Şimdilik bir tanecik node kaldıralım. 

`docker service create --name couchbase-worker --replicas 1 --network couchbase -e TYPE=WORKER -e COUCHBASE_MASTER=couchbase-master.couchbase -e AUTO_REBALANCE=false arungupta/couchbas`

Master'a verdiğimiz komuttan hiçbir farkı yok environment variable'lar dışında.

![2](https://i.ibb.co/FV2VxqT/2.png)

Buraya kadar sorunsuz geldiysek web panelini görmeyi murad etmeyi hak etmişiz demektir.

### Couchbase Web Panel
 Hadi dışarıya export ettiğimiz **8091** portuna browser'dan erişmeyi deneyelim.

![3](https://i.ibb.co/6HRMFNY/3.png)

ve bingo!

![4](https://i.ibb.co/1d2yWkR/4.png)

Nihayet erişebildik yönetim panelimize. 

### Scale Nodes
Sol alta bakarsanız relabalance edilmemiş 2 node'umuz varmış ve farz edelim ki yetmeyeceğini düşündük. E iyi de nasıl scale edeceğiz? Service'ı bu yüzden tanımladık zaten. Diyeceğimiz tek şey **couchbase-worker** servisindeki replica sayısını 3'e çıkar. Diyelim hemen.

`docker service scale couchbase-worker=3`

![5](https://i.ibb.co/h9rY21p/5.png)

Ve bingo! Görüldüğü üzere rebalance edilmemiş server sayısı **2'den 3'e** yükseldi. Hemen rebalance edip **bucket** yaratmaya geçelim.

![6](https://i.ibb.co/ZNmM3DL/6.png)

### Create a Bucket

Data Buckets tabımızdan bir bucket yarattık. 

![7](https://i.ibb.co/SXJCbC7/7.png)

Bir data yaratalım içinde bakalım. Örneğin trendyol.com'da görünecek bir product koyalım.

![8](https://i.ibb.co/6y1YgvG/any.png)

### Create a Index
Ve bir de index yaratalım products'larımız için.

![9](https://i.ibb.co/3MpFRhY/8.png)


### Case1'de daha fazla ayrıntı bulamadığım için case1'e ayrılan yazının sonuna geldik ='(

# Case - 2

Bu case'de bizden kurduğumuz sistem hakkında bilgi çekmemiz istendi. Ama ne tür bilgiler bu çok açık değil. Burdan benim çıkarımım cluster'a ait metadata bilgileri.


İki farklı çözüm sunmak istedim. Birincisi düz metadata veren API(**5001**), ikinci ise doğrudan Couchbase API'sine proxy yaparak size data sunan API(**5000**). 

Çalıştıralım; 

    ❯ python3 couchbase_api_proxy.py
	Started Proxy server on 5000
	Started Info server on 5001


## MetaData API

Metada olarak 3 enpoint'e sahibiz;

 - **/info/general** -> Doğrudan cluster metadatası döndürür.
 - **/info/buckets** -> Bucket metadatalarını döndürür.
 - **/info/nodes** -> Nodes metadatalarını göndürür.

### General Endpoint
`curl localhost:5001/info/general -s|jq -s`

`
[
  {
    "storageTotals": {
      "ram": {
        "total": 2084249600,
        "quotaTotal": 314572800,
        "quotaUsed": 314572800,
        "used": 1894641664,
        "usedByData": 49520904,
        "quotaUsedPerNode": 314572800,
        "quotaTotalPerNode": 314572800
      },
      "hdd": {
        "total": 62725623808,
        "quotaTotal": 62725623808,
        "used": 25090249523,
        "usedByData": 4269571,
        "free": 37635374285
      }
    },
    "ftsMemoryQuota": 512,
    "indexMemoryQuota": 300,
    "memoryQuota": 300,`
    .... 

### Buckets Enpoint

`curl localhost:5001/info/buckets -s |jq -s`

 `
[
  [
    {
      "name": "firstBucket",
      "bucketType": "membase",
      "authType": "sasl",
      "saslPassword": "",
      "proxyPort": 0,
      "replicaIndex": false,
      "uri": "/pools/default/buckets/firstBucket?bucket_uuid=dd0133a928f759ce35ad09e6b2ed6fb3",
      "streamingUri": "/pools/default/bucketsStreaming/firstBucket?bucket_uuid=dd0133a928f759ce35ad09e6b2ed6fb3",
      "localRandomKeyUri": "/pools/default/buckets/firstBucket/localRandomKey",
`
...

### Nodes Enpoint
`curl localhost:5001/info/nodes -s |jq -s`

`
[
  {
    "storageTotals": {
      "ram": {
        "total": 2084249600,
        "quotaTotal": 314572800,
        "quotaUsed": 314572800,
        "used": 1895116800,
        "usedByData": 49520904,
        "quotaUsedPerNode": 314572800,
        "quotaTotalPerNode": 314572800
      },
      "hdd": {
        "total": 62725623808,
        "quotaTotal": 62725623808,
        "used": 25090249523,
        "usedByData": 4269571,
        "free": 37635374285
      }
    },
    "ftsMemoryQuota": 512,
    "indexMemoryQuota": 300,
    "memoryQuota": 300,
    "name": "default",
    "alerts": [],
    "alertsSilenceURL": "/controller/resetAlerts?token=0&uuid=669ab9d45240f5381012ed7e9d7c7a13",
    "nodes": [
      {
        "systemStats": {
          "cpu_utilization_rate": 12.72727272727273,
          "swap_total": 1073737728,
          "swap_used": 1073647616,
          "mem_total": 2084249600,
          "mem_free": 392790016
        },
`
...

## API as Proxy 

**/info/general** adresine gittiğimizde aslında Couchbase'in **/pools/default** adresinden gelen datayı alıyorduk, yaptığı iş hala aynı yaklaşım farklı. **Bu proxy'i yazma amacımız yukarıda çekilmesi istenilen bilgilerin ucu açık olduğu için işi garantiye almak adına yazılmıştır.** Metadata API'si üç endpoint'e sahipken bu API, Couchbase'in sahip olduğu enpoint sayısı kadar endpoint'e sahiptir.

Örneğin yukarıdaki buckets endpoint'in elde ettiği datayı bir de bu API ile elde edelim.

` curl localhost:5000/pools/default/buckets -s -u Administrator:password | jq -s `

`
[
  {
    "name": "firstBucket",
    "bucketType": "membase",
    "authType": "sasl",
    "saslPassword": "",
    "proxyPort": 0,
    "replicaIndex": false,
    "uri": "/pools/default/buckets/firstBucket?bucket_uuid=dd0133a928f759ce35ad09e6b2ed6fb3",
    "streamingUri": "/pools/default/bucketsStreaming/firstBucket?bucket_uuid=dd0133a928f759ce35ad09e6b2ed6fb3",
    "localRandomKeyUri": "/pools/default/buckets/firstBucket/localRandomKey",
    "controllers": {
      "compactAll": "/pools/default/buckets/firstBucket/controller/compactBucket",
      "compactDB": "/pools/default/buckets/default/controller/compactDatabases",
      "purgeDeletes": "/pools/default/buckets/firstBucket/controller/unsafePurgeBucket",
      "startRecovery": "/pools/default/buckets/firstBucket/controller/startRecovery"
    },
    "nodes": [
      {
        "couchApiBaseHTTPS": "https://10.0.3.24:18092/firstBucket%2Bdd0133a928f759ce35ad09e6b2ed6fb3",
        "couchApiBase": "http://10.0.3.24:8092/firstBucket%2Bdd0133a928f759ce35ad09e6b2ed6fb3",
        "systemStats": {
          "cpu_utilization_rate": 3.655352480417755,
          "swap_total": 1073737728,
          "swap_used": 1073737728,
          "mem_total": 2084249600,
          "mem_free": 317325312
        }
`
...

Mevzu bahis [Couchbase Rest API](https://docs.couchbase.com/server/current/rest-api/rest-endpoints-all.html) dökümantasyonunda ne kadar endpoint varsa hepsine local üzerinden erişim sağlayıp, proxy edebiliriz. 